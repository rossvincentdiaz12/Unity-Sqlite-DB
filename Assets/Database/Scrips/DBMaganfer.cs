﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;


public class DBMaganfer : MonoBehaviour {

    private string connectionString;

    private List<HighScores> highScores = new List<HighScores>();

    public GameObject ScorePrefab;

    public Transform scoreParent;

    public int toprank;

    public int saveScore;

    public Text enterName;

    public GameObject nameDialog;

    public bool isPause;

	// Use this for initialization
	void Start () 
    {
        connectionString = "URI=file:" + Application.dataPath + "/Db_Score.sqlite";

       InsertScore("Sample",12);
        //DeleteScore(5);
        //GetScores();
       // DeleteExtraScore();
        ShowScores();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            nameDialog.SetActive(true);
        }
        

      
      }
	

    public void EnterData()
    {
        if(enterName.text !=string.Empty)
        {
            int score = UnityEngine.Random.Range(1, 20);
            InsertScore(enterName.text,score);
            enterName.text = string.Empty;

            ShowScores();
        }
    }

    private void InsertScore(string name, int newScore)
    {
        GetScores();
        int hsCount = highScores.Count;
        if(highScores.Count>0)
        {
            HighScores lowestScore = highScores[highScores.Count - 1];  
            if(lowestScore !=null && saveScore >0 && highScores.Count>=saveScore && newScore > lowestScore.Score)
            {
                DeleteScore(lowestScore.ID);
                hsCount--;
            }
        }
        if(hsCount  <saveScore)
        {

            using (IDbConnection dbConnection = new SqliteConnection(connectionString))
            {

                dbConnection.Open();
                using (IDbCommand dbCmd = dbConnection.CreateCommand())
                {
                    string sqlQuery = String.Format("INSERT INTO Tbl_Score(Name,Score)VALUES(\"{0}\",\"{1}\")", name, newScore);

                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();
                    dbConnection.Close();

                }

            }
        }

    }


    private void GetScores()
    {
        highScores.Clear();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {

            dbConnection.Open();
            using(IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = "SELECT * FROM Tbl_Score";

                dbCmd.CommandText = sqlQuery;

                using(IDataReader reader = dbCmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                     //Debug.Log(reader.GetString(1)+" "+ reader.GetInt32(2));
                        highScores.Add(new HighScores(reader.GetInt32(0),reader.GetInt32(2),reader.GetString(1),reader.GetDateTime(3)));
                    }
                    dbConnection.Close();
                    reader.Close();                  
                }
            }

        }

        highScores.Sort();

    }

    private void DeleteScore(int id)
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {

            dbConnection.Open();
            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = String.Format("DELETE FROM Tbl_Score WHERE PlayerID = \"{0}\"", id);

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbConnection.Close();

            }

        }
    }

    private void ShowScores()
    {
        GetScores();
        for (int i = 0; i < toprank; i++)    
        {

            if(i <= highScores.Count -1)
            {
                GameObject tmpObjec = Instantiate(ScorePrefab);

                HighScores tmpScore = highScores[i];

                tmpObjec.GetComponent<HighScoreScript>().SetScore(tmpScore.Name, tmpScore.Score.ToString(), "#" + (i + 1).ToString());

                tmpObjec.transform.SetParent(scoreParent);

                tmpObjec.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            }
        

        }
    }

    private void DeleteExtraScore()
    {

        GetScores();

        if(saveScore <= highScores.Count)
        {
            int deleteCount = highScores.Count - saveScore;
            highScores.Reverse();
            using (IDbConnection dbConnection = new SqliteConnection(connectionString))
            {

                dbConnection.Open();
                using (IDbCommand dbCmd = dbConnection.CreateCommand())
                {
                    for (int i = 0; i < deleteCount; i++)
                    {
                        string sqlQuery = String.Format("DELETE FROM Tbl_Score WHERE PlayerID = \"{0}\"", highScores[i].ID);

                        dbCmd.CommandText = sqlQuery;
                        dbCmd.ExecuteScalar();
                        dbConnection.Close();
                    }


                }

            }
        }

       
    }
}
